name := """couchbase-play-example"""

organization := "mirkof"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.6"

resolvers ++= Seq(
  Resolver.sonatypeRepo("snapshots"),
  Resolver.sonatypeRepo("releases")
)

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  "com.couchbase.client" % "java-client" % "2.1.3", // java based couchbase driver
  "io.reactivex" % "rxjava" % "1.0.12", // rxjava
  "io.reactivex" %% "rxscala" % "0.25.0", // rxscala adapters of rxjava
  "com.github.nscala-time" %% "nscala-time" % "2.0.0" // scala jodatime
)

libraryDependencies ++= Seq(
  "org.specs2" %% "specs2-core" % "3.6.1" % Test
  //"org.reactivestreams" % "reactive-streams-tck" % "1.0.0" % Test
)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

// compilation options
scalacOptions ++= Seq(
  "-deprecation",                     // Emit warning and location for usages of deprecated APIs.
  "-feature",                         // Emit warning and location for usages of features that should be imported explicitly.
  "-deprecation",                     // Emit warning and location for usages of deprecated APIs.
  "-unchecked",                       // Enable additional warnings where generated code depends on assumptions.
  "-language:existentials",           // Existential types (besides wildcard types) can be written and inferred
  "-language:higherKinds",            // Allow higher-kinded types
  "-language:implicitConversions",    // Allow definition of implicit functions called views
  //"-Xdev", 		                      // Indicates user is a developer
  "-Xlint:_",                         // Enable recommended additional warnings.
  //"-Xfatal-warnings", 							// Fail the compilation if there are any warnings
  //"-Xfuture", 											// Turn on future language features.
  //"-Yno-adapted-args", 							// Do not adapt an argument list (either by inserting () or creating a tuple) to match the receiver.
  "target:jvm-1.8",                   // Target platform for object files.
  "-encoding", "UTF-8",               // force encoding to UTF-8
  "-Yrangepos"                        // spec2 option
)