package couchbase

import com.couchbase.client.java.CouchbaseAsyncCluster
import hailstorm.helpers._
import hailstorm.modules.HailstormConfModule
import hailstorm.settings.HailstormSettings
import org.specs2.concurrent.ExecutionEnv
import org.specs2.specification.BeforeAfterAll
import play.api.Configuration
import scaldi.Injector

import scala.collection.JavaConverters._

abstract class CouchbaseSpec(implicit ee: ExecutionEnv)
    extends HailstormSpecification
    with HailstormTestSettings
    with AwaitForResult
    with RandomFactory
    with BeforeAfterAll {

  final lazy val BucketName = config.couchbase.bucketName
  final lazy val user = config.couchbase.admin.user
  final lazy val pass = config.couchbase.admin.password

  var cluster: AsyncCluster = _
  var clusterManager: AsyncClusterManager = _
  var bucket: AsyncBucket = _
  var bucketManager: AsyncBucketManager = _

  override def beforeAll: Unit = {
    cluster = new AsyncCluster(CouchbaseAsyncCluster.create(config.couchbase.hosts.asJava))
    clusterManager = cluster.clusterManager(user, pass).awaitFor(longAwaitTimeout)
    bucket = cluster.openBucket(BucketName).awaitFor(longAwaitTimeout)
    bucketManager = bucket.manager.awaitFor(longAwaitTimeout)
  }

  override def afterAll: Unit = {
    bucket.close()
  }

}
