package couchbase

import couchbase.utils.RxPlayConversions
import play.api.libs.iteratee.Enumeratee
import org.specs2.concurrent._
import scala.util.Random

class AsyncClusterManagerSpec(implicit ee: ExecutionEnv) extends CouchbaseSpec {
  // scalastyle:off public.methods.have.type
  // NOTE: we are not using s2 interpolation because of BucketName
  def is = sequential ^ p ^ t ^
    "An AsyncCluster should" ^ br ^ t(2) ^
    s"check if bucket:$BucketName exists" ! e1 ^ br ^
    s"open bucket:$BucketName" ! e2 ^ p ^ bt ^
    "An AsyncClusterManager should" ^ br ^ t(2) ^
    "create a bucket" ! e3 ^ br ^
    "check if created bucket exists" ! e4 ^ br ^
    "get bucket settings" ! e5 ^ br ^
    "update bucket settings" ! e6 ^ br ^
    "remove a bucket" ! e7 ^ br ^
    end

  val testBucketName = "test-bucket" + Random.nextInt(Int.MaxValue)
  val bucketSettings = BucketSettings(
    name = testBucketName,
    quota = 100,
    enableFlush = true
  )

  def e1 = {
    clusterManager.hasBucket(BucketName) must beTrue.awaitFor(longAwaitTimeout)
  }

  def e2 = {
    cluster.openBucket(BucketName).map(_.name) must beEqualTo(BucketName).awaitFor(longAwaitTimeout)
  }

  def e3 = {
    clusterManager.insertBucket(bucketSettings) must beEqualTo(bucketSettings).awaitFor(longAwaitTimeout)
  }

  def e4 = {
    clusterManager.hasBucket(testBucketName) must beTrue.awaitFor(longAwaitTimeout)
  }

  def e5 = {
    clusterManager.getBucket(testBucketName) must beEqualTo(bucketSettings).awaitFor(longAwaitTimeout)
  }

  def e6 = {
    val newSettings = bucketSettings.copy(quota = 101)

    clusterManager.updateBucket(newSettings) must beEqualTo(newSettings).awaitFor(longAwaitTimeout)

    clusterManager.getBucket(testBucketName) must beEqualTo(newSettings).awaitFor(longAwaitTimeout)
  }

  def e7 = {
    clusterManager.removeBucket(testBucketName) must beTrue.awaitFor(longAwaitTimeout + longAwaitTimeout)

    clusterManager.hasBucket(testBucketName) must beFalse.awaitFor(longAwaitTimeout)
  }
}
