package couchbase

import com.couchbase.client.java.error.{ CASMismatchException, DocumentAlreadyExistsException }
import couchbase.documents.{ Document, JsDocument }
import couchbase.types.CouchbaseID
import org.specs2.concurrent._

class AsyncBucketSpec(implicit ee: ExecutionEnv) extends CouchbaseSpec {
  // scalastyle:off public.methods.have.type
  def is =
    s2"""
    A AsyncBucketManager should
      allow a document to be inserted                           $e1
      fail to insert document twice                             $e2
      be able to get a document                                 $e3
      fail to get a non existing document                       $e4
      allow a document to be updated                            $e5
      allow a document to be removed                            $e6
      support CAS failure on replace                            $e7
      """

  def e1 = {
    val doc: JsDocument = randomDoc("testInsert")

    bucket.insert(doc).map(_.content) must beEqualTo(doc.content).await
  }

  def e2 = {
    val doc: JsDocument = randomDoc("testInsert2")

    bucket.insert(doc).map(_.content) must beEqualTo(doc.content).await

    bucket.insert(doc) must throwA[DocumentAlreadyExistsException].await
  }

  def e3 = {
    val doc: JsDocument = randomDoc("testGet")

    bucket.insert(doc).map(_.content) must beEqualTo(doc.content).await

    bucket.get(doc.id).map(_.flatMap(_.content)) must beEqualTo(doc.content).await
  }

  def e4 = {
    val id: CouchbaseID = "testFailGet"

    bucket.get(id) must beNone.await
  }

  def e5 = {
    val doc: JsDocument = randomDoc("testUpdate", 1)

    // Insert a document containing 1 and check it's created
    bucket.insert(doc).map(_.content) must beEqualTo(doc.content).await

    val newDoc: JsDocument = Document(doc.id, Option(2), doc.cas)
    // Replace it with one containing 2
    bucket.replace(newDoc).map(_.content) must beEqualTo(newDoc.content).await

    // check that it really worked
    bucket.get(doc.id).map(_.flatMap(_.content)) must beEqualTo(newDoc.content).await
  }

  def e6 = {
    val doc: JsDocument = randomDoc("testRemove")

    bucket.insert(doc).map(_.content) must beEqualTo(doc.content).await

    bucket.remove(doc.id).await

    bucket.get(doc.id) must beNone.await
  }

  def e7 = {
    val doc: JsDocument = randomDoc("testCASReplace", 1)
    val result = bucket.insert(doc).await
    result.content must beEqualTo(doc.content)

    val updateDoc: JsDocument = Document(result.id, Option(2), result.cas)
    val updateResult = bucket.replace(updateDoc).await
    updateResult.content must beEqualTo(updateDoc.content)

    val failDoc: JsDocument = Document(result.id, Option(3), result.cas)

    bucket.replace(failDoc) must throwA[CASMismatchException].await
  }

}
