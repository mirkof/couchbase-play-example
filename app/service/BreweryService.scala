package service

import javax.inject.Inject

import couchbase.documents.Document
import couchbase.types.CouchbaseID
import couchbase.utils.RxPlayConversions
import couchbase.utils.RxPlayConversions._
import couchbase.view.ViewQuery
import models.{Beer, Brewery}
import play.api.libs.json._
import play.api.libs.json.util._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class BreweryService @Inject()(database: Database) {

  val bucket = database.bucket

  import Beer._
  import Brewery._
  import Document._

  def findById(id: CouchbaseID): Future[Document[Brewery]] = bucket.get(id).map(_.map(jsDocument2Document[Brewery]).getOrElse(Document(id, None)))

  def findByIdWithBeers(id: CouchbaseID): Future[Option[Document[Brewery]]] = {

    val queryBrewery = ViewQuery("beer", "brewery_beers")
      .startKey(id.value)
      //the trick here is that sorting is UTF8 based, uefff is the largest UTF8 char
      .endKey(JsArray(Seq(id.value, "\uefff").map(JsString)))

    val eventualDocuments = bucket.query(queryBrewery).flatMap { result =>
      enumerator2Seq(result.rows.map(_.document)).flatMap { listOfFutures =>
        Future.sequence(listOfFutures)
      }
    }

    eventualDocuments.map { documents =>

      val (breweryDocuments, beerDocuments) = documents.span(_.content.exists(_.as[JsObject].fieldSet.contains(("type", JsString("brewery")))))

      if (breweryDocuments.isEmpty) {
        None
      } else {
        val breweryDocument: Document[Brewery] = breweryDocuments.map(jsDocument2Document[Brewery]).head

        val beers: Seq[Beer] = beerDocuments.map(jsDocument2Document[Beer]).flatMap(_.content)

        val richBrewery: Option[Brewery] = breweryDocument.content.map(_.copy(beers = Option(beers)))

        Option(Document(breweryDocument.id, richBrewery, breweryDocument.cas, breweryDocument.expiry))
      }

    }


  }


}
