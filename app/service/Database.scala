package service

import javax.inject._

import com.couchbase.client.java.{CouchbaseAsyncCluster, CouchbaseCluster}
import com.couchbase.client.java.env.{CouchbaseEnvironment, DefaultCouchbaseEnvironment}
import com.couchbase.client.java.view.{DefaultView, DesignDocument}
import couchbase.{AsyncBucket, AsyncCluster, BucketSettings}
import couchbase.utils.RxPlayConversions._
import play.api.Play._
import play.api.inject.ApplicationLifecycle
import utils.LazyLogging

import scala.collection.JavaConverters._
import scala.collection.JavaConverters._
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import couchbase.view.View._
import couchbase.view.View

@Singleton
class Database @Inject()(lifecycle: ApplicationLifecycle) extends LazyLogging {
  final val BucketName = configuration.getString("couchbase.bucket").getOrElse("beer-sample")
  final val Hosts = configuration.getStringSeq("couchbase.hosts").getOrElse(Seq("localhost"))
  final val User = configuration.getString("couchbase.admin.user").getOrElse("Administrator")
  final val Pass = configuration.getString("couchbase.admin.pass").getOrElse("password")
  final val LongWaitTime = 30.seconds
  
  private val byNameView:View = DefaultView.create("by_name",
    s"""function (doc, meta) {
       | if (doc.type == "beer") {
       |  emit(doc.name, doc.brewery_id)
       | }
       |}
    """.stripMargin
  )
  private var _maybeEnvironment: Option[CouchbaseEnvironment] = None
  private var _maybeCluster: Option[AsyncCluster] = None
  private var _maybeBucket: Option[AsyncBucket] = None

  def environment: CouchbaseEnvironment =
    this.synchronized {
      _maybeEnvironment.getOrElse(createEnvironment())
    }

  def cluster: AsyncCluster =
    this.synchronized {
      _maybeCluster.getOrElse(createCluster())
    }

  def bucket: AsyncBucket =
    this.synchronized {
      _maybeBucket.getOrElse(openBucket())
    }

  private def openBucket(): AsyncBucket = {
    this.synchronized {
      log.info(s"Trying to access bucket: $BucketName")
      val eventualBucket = cluster.openBucket(BucketName)

      val bucket = Await.result(eventualBucket, LongWaitTime)
      _maybeBucket = Option(bucket)


      // check if all the view exist
      def findByNameView(doc:DesignDocument):Option[View] = doc.views.asScala.map(toScalaView).find(_.name == byNameView.name)

      bucket.manager.map { manager =>
        manager.getDesignDocument("beer").map { beerDesignDocument =>
          val maybeByNameView = findByNameView(beerDesignDocument)

          if(maybeByNameView.isEmpty){
            manager.upsertDesignDocument(beerDesignDocument.copy(views =  byNameView +: beerDesignDocument.views)).map { doc =>
              assert(findByNameView(doc).isDefined)
            }
          }
        }
      }
      
      
      bucket
    }
  }

  private def createCluster(): AsyncCluster =
    this.synchronized {
      log.info(s"Connecting to hosts: $Hosts")
      val cluster = new AsyncCluster(CouchbaseAsyncCluster.create(environment, Hosts.asJava))
      _maybeCluster = Option(cluster)
      cluster
    }

  private def createEnvironment() =
    this.synchronized {
      val procs = Runtime.getRuntime.availableProcessors()

      val environment = DefaultCouchbaseEnvironment.builder()
        .kvTimeout(30.seconds.toMillis)
        .viewTimeout(30.seconds.toMillis)
        .kvEndpoints(3) // Yep we can saturate 1 so set it to 3
        .viewEndpoints(4)
        .requestBufferSize(32 * 1024) // 32k ring buffer = more requests/second
        .autoreleaseAfter(3.seconds.toMillis) // increase the release timer after unsubscribing
        .build()

      log.debug(s"createEnvironment: Couchbase Environment: $environment")
      _maybeEnvironment = Option(environment)
      environment
    }

  private def createBucketSettings() = BucketSettings(name = BucketName, quota = 100, enableFlush = true)

  lifecycle.addStopHook { () =>
    bucket.close().map(_ => Unit)
  }

}
