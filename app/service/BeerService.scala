package service

import javax.inject.Inject

import couchbase.documents.Document
import couchbase.types.CouchbaseID
import couchbase.utils.RxPlayConversions
import couchbase.view.{AsyncViewResult, ViewQuery}
import models.Beer
import play.api.libs.iteratee.{Enumeratee, Enumerator}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


class BeerService @Inject()(database: Database) {
  val bucket = database.bucket

  import Beer._
  import Document._

  def findById(id: CouchbaseID): Future[Document[Beer]] = bucket.get(id).map(_.map(jsDocument2Document[Beer]).getOrElse(Document(id, None)))

  def update(document: Document[Beer]): Future[Document[Beer]] = bucket.upsert(document).map(jsDocument2Document[Beer])

  def delete(id: CouchbaseID): Future[Document[Beer]] = bucket.remove(id).map(jsDocument2Document[Beer])

  import RxPlayConversions.enumerator2Seq

  private implicit def viewResult2SeqDocument(asyncResult: Future[AsyncViewResult]): Future[Seq[Document[Beer]]] = {
    asyncResult.flatMap { result =>
      enumerator2Seq(result.rows.map(_.document.map(jsDocument2Document[Beer]))).flatMap { listOfFutures =>
        Future.sequence(listOfFutures)
      }
    }
  }

  def findAll(offset: Int, limit: Int): Future[Seq[Document[Beer]]] = {
    require(limit >= 0, "The limit must not be negative.")
    require(offset >= 0, "The offset must not be negative.")

    val query = ViewQuery("beer", "by_name")
      .limit(limit)
      .skip(offset)

    bucket.query(query)
  }

  def findAll: Future[Seq[Document[Beer]]] = {
    val query = ViewQuery("beer", "by_name")
    bucket.query(query)
  }


  def findAllByName(name: String): Future[Seq[Document[Beer]]] = {
    val query = ViewQuery("beer", "by_name")

    val eventualEnumBeerDocuments: Future[Enumerator[Document[Beer]]] = bucket.query(query).map { result =>
      result.rows.map(_.document.map(jsDocument2Document[Beer])) through Enumeratee.mapM(identity)
    }

    val filterByNameEnumeratee: Enumeratee[Document[Beer], Document[Beer]] = Enumeratee.filter[Document[Beer]] { document =>
      document.content.exists { beer =>
        beer.name.toLowerCase.contains(name.toLowerCase)
      }
    }

    eventualEnumBeerDocuments.flatMap { enumBeerDocuments =>
      enumerator2Seq(enumBeerDocuments through filterByNameEnumeratee)
    }

  }


}
