package models

import com.github.nscala_time.time.Imports.DateTime
import couchbase.documents.Document
import couchbase.utils.JodaConverters
import play.api.libs.json.Json
import JodaConverters._

case class Beer(name: String,
                abv: Option[Double],
                ibu: Option[Double],
                srm: Option[Double],
                upc: Option[Double],
                `type`: String,
                brewery_id: Option[String],
                updated: DateTime = DateTime.now,
                description: Option[String],
                style: Option[String],
                category: Option[String]
                 )


object Beer {

  implicit val beerFormat = Json.format[Beer]
}


