package models


import com.github.nscala_time.time.Imports.DateTime
import couchbase.documents.Document
import couchbase.utils.JodaConverters
import play.api.libs.json.Json
import JodaConverters._

case class Brewery(name: String,
                   code: Option[String],
                   description: Option[String],
                   address: Option[Seq[String]],
                   city: Option[String],
                   state: Option[String],
                   country: Option[String],
                   geo: Option[LatLang],
                   phone: Option[String],
                   website: Option[String],
                   `type`: String,
                   updated: DateTime,
                   beers: Option[Seq[Beer]])

case class LatLang(accuracy: String, lon: Double, lat: Double)


object LatLang {
  implicit val latLangFormat = Json.format[LatLang]
}

object Brewery {
  implicit val breweryFormat = Json.format[Brewery]
}



