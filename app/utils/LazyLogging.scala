package utils

import play.api.Logger

trait LazyLogging {
  protected lazy val log = Logger(getClass)
}
