package controllers


import javax.inject.Inject


import com.couchbase.client.java.view.DefaultView
import play.api.libs.json.Json
import play.api.mvc._
import service.Database
import couchbase.view.View._
import scala.collection.JavaConverters._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class Application @Inject()(database: Database) extends Controller {

  def index = Action {
    Ok(views.html.index())
  }

  def validateDocuments = Action.async {
    import couchbase.view.{View => ScalaView}
    import ScalaView._

    database.bucket.manager.flatMap { manager =>
      manager.getDesignDocument("beer").map { beerDesignDocument =>
        val views = beerDesignDocument.views.map { view =>
          DefaultView.create(view.name, view.map.substring(0, view.map.lastIndexOf("}")))
        }.map(toScalaView)

        Ok(Json.toJson(views))
      }
    }
  }

}