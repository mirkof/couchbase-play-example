package controllers

import javax.inject.Inject

import play.api.libs.json.Json
import play.api.mvc.{Action, Controller}
import service.{BeerService, BreweryService}
import play.api.libs.concurrent.Execution.Implicits.defaultContext

class BreweryController @Inject()(breweryService: BreweryService, beerService: BeerService) extends Controller {

  def getById(id: String) = Action.async { implicit request =>
    breweryService.findByIdWithBeers(id).map { breweryDocument =>
      breweryDocument.map { breweryDocument =>
        breweryDocument.content.map { brewery =>
          Ok(Json.toJson(brewery))
        }.getOrElse{
          NotFound
        }
      }.getOrElse{
        NotFound
      }
    }

  }


}
