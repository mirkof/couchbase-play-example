package controllers

import javax.inject.Inject

import com.github.nscala_time.time.Imports._
import couchbase.documents.Document
import models.Beer
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.{JsError, Json}
import play.api.mvc.{Action, Controller}
import service.BeerService

import scala.concurrent.Future

class BeerController @Inject()(beerService: BeerService) extends Controller {


  def getById(id: String) = Action.async { implicit request =>
    beerService.findById(id).map { doc =>
      doc.content.map { beer =>
        Ok(Json.toJson(beer))
      }.getOrElse{
        NotFound
      }
    }
  }

  def update(id: String) = Action.async { implicit request =>
    request.body.asJson.map { json =>
      json.validate[Beer].map { beer =>
        val doc = Document[Beer](id, Option(beer.copy(updated = DateTime.now)))
        beerService.update(doc).map { d =>
          Ok(Json.toJson(d.content))
        }
      }.recoverTotal { e =>
        Future.successful(BadRequest("Detected error:" + JsError.toJson(e)))
      }
    }.getOrElse {
      Future.successful(BadRequest("Expecting Json data"))
    }
  }


  def delete(id: String) = Action.async { implicit request =>
    beerService.delete(id).map { d =>
      Ok(Json.toJson(d.content))
    }
  }

  def list = Action.async { implicit request =>
    val limit = request.getQueryString("limit").map(_.toInt).getOrElse(10)
    val skip = request.getQueryString("skip").map(_.toInt).getOrElse(0)

    beerService.findAll(skip, limit).map { beers =>
      Ok(Json.toJson(beers.map(_.content)))
    }
  }

  def search(name:String) = Action.async { implicit request =>
    beerService.findAllByName(name).map { beers =>
      Ok(Json.toJson(beers.map(_.content)))
    }
  }

}
