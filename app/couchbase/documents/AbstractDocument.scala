package couchbase.documents

import couchbase.types._

abstract class AbstractDocument[A](
    id:      CouchbaseID,
    content: Option[A],
    cas:     Option[CAS]    = None,
    expiry:  Option[Expiry] = None
) extends Document[A] {

  require(id.value.nonEmpty, "The Document ID must not be null or empty.")
  require(id.value.getBytes.length <= 250, "The Document ID must not be larger than 250 bytes")
  require(expiry >= 0, "The Document expiry must not be negative.")
}
