package couchbase.documents

import com.couchbase.client.java.document.JsonLongDocument
import couchbase.types._
import play.api.libs.json._

case class LongDocument private (
  id:      CouchbaseID,
  content: Option[Long],
  cas:     Option[CAS]    = None,
  expiry:  Option[Expiry] = None
) extends AbstractDocument[Long](id, content, cas, expiry)

object LongDocument {

  implicit val jsLongDocumentFormat: Format[LongDocument] = Json.format[LongDocument]

  implicit def fromJsonLongDocument(json: JsonLongDocument): LongDocument = LongDocument(json.id, Option(json.content.toLong), json.cas, json.expiry)

  implicit def toJsonLongDocument(js: LongDocument): JsonLongDocument = {
    if (js.content.isDefined) {
      JsonLongDocument.create(js.id, js.expiry, js.content.get, js.cas)
    } else {
      JsonLongDocument.create(js.id)
    }
  }

}
