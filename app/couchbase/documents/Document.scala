package couchbase.documents

import couchbase.types._
import play.api.libs.json._

trait Document[A] {

  def id: CouchbaseID

  def content: Option[A]

  def cas: Option[CAS]

  def expiry: Option[Expiry]
}

sealed case class DefaultDocument[A](
  id:      CouchbaseID,
  content: Option[A],
  cas:     Option[CAS]    = None,
  expiry:  Option[Expiry] = None
) extends AbstractDocument[A](id, content, cas, expiry)

object Document {
  def apply[A](
    id:      CouchbaseID,
    content: Option[A],
    cas:     Option[CAS]    = None,
    expiry:  Option[Expiry] = None
  ): Document[A] = new DefaultDocument[A](id, content, cas, expiry)

  def apply[A](id: CouchbaseID, content: A): Document[A] = new DefaultDocument[A](id, Option(content))

  implicit def document2JsDocument[A](doc: Document[A])(implicit wra: Writes[A]): JsDocument =
    JsDocument(doc.id, doc.content.flatMap(x => Json.toJson[A](x).asOpt[JsValue]), doc.cas, doc.expiry)

  implicit def jsDocument2Document[A](jsDoc: JsDocument)(implicit rda: Reads[A]): Document[A] =
    new DefaultDocument(jsDoc.id, jsDoc.content.flatMap(js => Json.fromJson[A](js).asOpt), jsDoc.cas, jsDoc.expiry)

}
