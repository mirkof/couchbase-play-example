package couchbase.types

import play.api.libs.json.Json

case class Expiry(value: Int) extends AnyVal

object Expiry {

  implicit def fromExpiryOpt(expiry: Option[Expiry]): Int = expiry.getOrElse(Expiry(0)).value

  implicit def toExpiryOpt(value: Int): Option[Expiry] = if (value > 0) Option(Expiry(value)) else None

  implicit val expiryFormat = Json.format[Expiry]

}
