package couchbase.types

import play.api.libs.json.{JsString, JsValue, Writes}

case class ViewName(value: String) extends AnyVal {
  override def toString: String = value
}

object ViewName {
  implicit def stringToDesignDocumentName(value: String): ViewName = new ViewName(value)

  implicit def designDocumentNameToString(name: ViewName): String = name.value

  implicit val viewNameWrites = new Writes[ViewName] {
    def writes(o: ViewName): JsValue = JsString(o.value)
  }

}
