package couchbase

import com.couchbase.client.java.bucket.{ AsyncBucketManager => JavaAsyncBucketManager, BucketInfo }
import couchbase.utils.RxPlayConversions._
import couchbase.view.{ DesignDocument, DesignDocumentMode, ProductionMode }
import play.api.libs.iteratee.Enumerator

import scala.concurrent.{ ExecutionContext, Future }

class AsyncBucketManager(bucketManager: JavaAsyncBucketManager) {

  def info(implicit ec: ExecutionContext): Future[BucketInfo] = observable2Future(bucketManager.info)

  def flush(implicit ec: ExecutionContext): Future[Boolean] = observable2FutureWithConvert(bucketManager.flush)

  def getDesignDocuments(mode: DesignDocumentMode = ProductionMode)(implicit ec: ExecutionContext): Enumerator[DesignDocument] =
    observable2EnumeratorWithConvert(bucketManager.getDesignDocuments(mode.value))

  def getDesignDocument(name: String, mode: DesignDocumentMode = ProductionMode)(implicit ec: ExecutionContext): Future[DesignDocument] =
    observable2FutureWithConvert(bucketManager.getDesignDocument(name, mode.value))

  def insertDesignDocument(designDocument: DesignDocument, mode: DesignDocumentMode = ProductionMode)(implicit ec: ExecutionContext): Future[DesignDocument] =
    observable2FutureWithConvert(bucketManager.insertDesignDocument(designDocument, mode.value))

  def upsertDesignDocument(designDocument: DesignDocument, mode: DesignDocumentMode = ProductionMode)(implicit ec: ExecutionContext): Future[DesignDocument] =
    observable2FutureWithConvert(bucketManager.upsertDesignDocument(designDocument, mode.value))

  def removeDesignDocument(name: String, mode: DesignDocumentMode = ProductionMode)(implicit ec: ExecutionContext): Future[Boolean] =
    observable2FutureWithConvert(bucketManager.removeDesignDocument(name, mode.value))

  def publishDesignDocument(name: String, overwrite: Boolean = false)(implicit ec: ExecutionContext): Future[DesignDocument] =
    observable2FutureWithConvert(bucketManager.publishDesignDocument(name, overwrite))

}

object AsyncBucketManager {
  implicit def toScalaBucketManager(manager: JavaAsyncBucketManager): AsyncBucketManager = new AsyncBucketManager(manager)
}

