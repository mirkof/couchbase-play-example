package couchbase.utils

import play.api.libs.iteratee.Concurrent.Channel
import play.api.libs.iteratee._
import rx.lang.scala.JavaConversions._
import rx.lang.scala._
import rx.lang.scala.schedulers.ExecutionContextScheduler

import scala.concurrent.{ ExecutionContext, Future, Promise }
import scala.util.{ Failure, Success }

/**
 * Helper class for converting [[rx.Observable]] to [[Enumerator]] and [[Future]]
 *
 * See:
 * http://bryangilbert.com/blog/2013/10/22/rxPlay-making-iteratees-and-observables-play-nice/
 * http://bryangilbert.com/blog/2013/11/03/rx-the-importance-of-honoring-unsubscribe/
 */
object RxPlayConversions {

  def observable2Future[T](obs: rx.Observable[_ <: T])(implicit ec: ExecutionContext): Future[T] = {
    val promise = Promise[T]()

    toScalaObservable(obs)
      .subscribeOn(ExecutionContextScheduler(ec))
      .subscribe(
        onNext => promise.success(onNext),
        e => promise.failure(e)
      )

    promise.future
  }

  def enumerator2Observable[T](enum: Enumerator[T])(implicit ec: ExecutionContext): rx.Observable[_ <: T] = {
    // creating the Observable that we return
    val obs = Observable({ observer: Observer[T] =>
      // keeping a way to unsubscribe from the observable
      var cancelled = false

      // enumerator input is tested with this predicate
      // once cancelled is set to true, the enumerator will stop producing data
      val cancellableEnum = enum through Enumeratee.breakE[T](_ => cancelled)

      // applying iteratee on producer, passing data to the observable
      cancellableEnum(
        Iteratee.foreach(observer.onNext(_))
      ).onComplete { // passing completion or error to the observable
          case Success(_) => observer.onCompleted()
          case Failure(e) => observer.onError(e)
        }

      // unsubscription will change the var to stop the enumerator above via the breakE function
      new Subscription { override def unsubscribe() = { cancelled = true } }
    })

    toJavaObservable[T](obs.subscribeOn(ExecutionContextScheduler(ec)))
  }

  def observable2Enumerator[T](obs: rx.Observable[T])(implicit ec: ExecutionContext): Enumerator[T] = {
    // unicast create a channel where you can push data and returns an Enumerator
    Concurrent.unicast { channel =>
      val subscription = obs.subscribeOn(ExecutionContextScheduler(ec)).subscribe(ChannelObserver(channel))
      val onComplete = { () => subscription.unsubscribe() }
      val onError = { (_: String, _: Input[T]) => subscription.unsubscribe() }
      (onComplete, onError)
    }
  }

  case class ChannelObserver[T](chan: Channel[T]) extends Observer[T] {
    override def onNext(elem: T): Unit = chan.push(elem)

    override def onCompleted(): Unit = chan.end()

    override def onError(e: Throwable): Unit = chan.end(e)
  }

  def enumerator2Seq[T](enum: Enumerator[T])(implicit ec: ExecutionContext): Future[Seq[T]] = {
    enum run Iteratee.fold(List.empty[T]) { (l, e) => e :: l } map { _.reverse }
  }

  def observable2FutureWithConvert[A, B](obs: rx.Observable[_ <: A])(implicit convert: (A) => B, ec: ExecutionContext): Future[B] = {
    observable2Future(obs).map(convert)
  }

  def observable2FutureOptionWithConvert[A, B](obs: rx.Observable[_ <: A])(implicit convert: (A) => B, ec: ExecutionContext): Future[Option[B]] = {
    import rx.lang.scala.ImplicitFunctionConversions.scalaFunction1ToRxFunc1
    observable2Future {
      obs.map[Option[B]]((a: A) => Option(convert(a)))
        .switchIfEmpty(rx.Observable.just(None))
    }
  }

  def enumerator2ObservableWithConvert[B, A](enum: Enumerator[B])(implicit convert: (B) => A, ec: ExecutionContext): rx.Observable[_ <: A] = {
    enumerator2Observable(enum.map(convert))
  }

  def observable2EnumeratorWithConvert[A, B](obs: rx.Observable[_ <: A])(implicit convert: (A) => B, ec: ExecutionContext): Enumerator[B] = {
    observable2Enumerator(obs).map(convert)(ec)
  }
}
