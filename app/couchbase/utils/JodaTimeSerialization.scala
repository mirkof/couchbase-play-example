package couchbase.utils

import com.github.nscala_time.time.Imports._
import org.joda.time.format.ISODateTimeFormat
import play.api.data.validation.ValidationError
import play.api.libs.json._

object JodaTimeSerialization {
  DateTimeZone.setDefault(DateTimeZone.UTC)

  val fmt = ISODateTimeFormat.dateTime

  /**
   * DateTime Json formats
   */
  implicit val dateTimeFormat = new Format[DateTime] {
    override def writes(o: DateTime): JsValue = JsString(fmt print o)

    override def reads(json: JsValue): JsResult[DateTime] = json match {
      case JsNumber(value) => JsSuccess(new DateTime(value.toLong))
      case JsString(value) => JsSuccess(fmt parseDateTime value)
      case _               => JsError(Seq(JsPath() -> Seq(ValidationError("validate.error.expected.datetime"))))
    }
  }

  /**
   * LocalDate Json formats
   */
  implicit val localDateFormat = new Format[LocalDate] {
    override def writes(o: LocalDate): JsValue = JsString(fmt print o)

    override def reads(json: JsValue): JsResult[LocalDate] = json match {
      case JsString(value) => JsSuccess(fmt parseLocalDate value)
      case _               => JsError(Seq(JsPath() -> Seq(ValidationError("validate.error.expected.localdate"))))
    }
  }

  /**
   * Duration Json formats
   */
  implicit val durationFormat = new Format[Duration] {
    override def writes(o: Duration): JsValue = JsNumber(o.getMillis)

    override def reads(json: JsValue): JsResult[Duration] = json match {
      case JsNumber(value) => JsSuccess(new Duration(value.toLong))
      case _               => JsError(Seq(JsPath() -> Seq(ValidationError("validate.error.expected.duration"))))
    }
  }

}
