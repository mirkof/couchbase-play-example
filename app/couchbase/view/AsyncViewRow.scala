package couchbase.view

import com.couchbase.client.java.view.{ AsyncViewRow => JavaAsyncViewRow }
import couchbase.documents.JsDocument
import JsDocument._
import couchbase.utils.RxPlayConversions
import RxPlayConversions.observable2FutureWithConvert

import scala.concurrent.{ ExecutionContext, Future }

case class AsyncViewRow(
  id:       String,
  key:      Any,
  value:    Any,
  document: Future[JsDocument]
)

object AsyncViewRow {

  implicit def toScalaAsyncViewRow(row: JavaAsyncViewRow)(implicit ec: ExecutionContext): AsyncViewRow =
    AsyncViewRow(row.id(), row.key(), row.value(), observable2FutureWithConvert(row.document()))
}
