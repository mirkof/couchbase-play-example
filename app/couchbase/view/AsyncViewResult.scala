package couchbase.view

import com.couchbase.client.java.view.{ AsyncViewResult => JavaAsyncViewResult }
import couchbase.utils.{ RxPlayConversions, JsonConversions }
import play.api.libs.iteratee.Enumerator
import play.api.libs.json.{ JsValue, JsObject }
import JsonConversions._
import couchbase.view.AsyncViewRow._
import RxPlayConversions.observable2EnumeratorWithConvert
import scala.concurrent.ExecutionContext

case class AsyncViewResult(
  rows:      Enumerator[AsyncViewRow],
  totalRows: Integer,
  success:   Boolean,
  errors:    Enumerator[Option[JsValue]],
  debug:     Option[JsValue]
)

object AsyncViewResult {

  implicit def toScalaAsyncViewResult(result: JavaAsyncViewResult)(implicit ec: ExecutionContext): AsyncViewResult =
    AsyncViewResult(
      rows = observable2EnumeratorWithConvert(result.rows()),
      totalRows = result.totalRows(),
      success = result.success(),
      errors = observable2EnumeratorWithConvert(result.error()),
      debug = result.debug()
    )
}
