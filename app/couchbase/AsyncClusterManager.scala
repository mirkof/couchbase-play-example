package couchbase

import com.couchbase.client.java.cluster.{ AsyncClusterManager => JavaAsyncClusterManager, ClusterInfo }
import couchbase.BucketSettings._
import couchbase.utils.RxPlayConversions._
import play.api.libs.iteratee.{ Enumeratee, Enumerator }

import scala.concurrent.{ ExecutionContext, Future }

class AsyncClusterManager(manager: JavaAsyncClusterManager) {

  def info(implicit ec: ExecutionContext): Future[ClusterInfo] =
    observable2Future(manager.info)

  def getBuckets(implicit ec: ExecutionContext): Enumerator[BucketSettings] =
    observable2EnumeratorWithConvert(manager.getBuckets)

  def getBucket(name: String)(implicit ec: ExecutionContext): Future[BucketSettings] =
    observable2FutureWithConvert(manager.getBucket(name))

  def hasBucket(name: String)(implicit ec: ExecutionContext): Future[Boolean] =
    for {
      has <- observable2FutureWithConvert(manager.hasBucket(name))
      list <- enumerator2Seq(this.getBuckets &> Enumeratee.filter(_.name == name))
    } yield has && list.nonEmpty

  def insertBucket(settings: BucketSettings)(implicit ec: ExecutionContext): Future[BucketSettings] =
    observable2FutureWithConvert(manager.insertBucket(settings))

  def updateBucket(settings: BucketSettings)(implicit ec: ExecutionContext): Future[BucketSettings] =
    observable2FutureWithConvert(manager.updateBucket(settings))

  def removeBucket(name: String)(implicit ec: ExecutionContext): Future[Boolean] =
    observable2FutureWithConvert(manager.removeBucket(name))

}

object AsyncClusterManager {
  implicit def toScalaClusterManager(manager: JavaAsyncClusterManager): AsyncClusterManager = new AsyncClusterManager(manager)
}
