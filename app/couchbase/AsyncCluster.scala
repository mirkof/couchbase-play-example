package couchbase

import com.couchbase.client.java.{ AsyncCluster => JavaAsyncCluster }
import couchbase.utils.RxPlayConversions.observable2FutureWithConvert

import scala.concurrent.{ ExecutionContext, Future }

class AsyncCluster(cluster: JavaAsyncCluster) {

  def openBucket(implicit ec: ExecutionContext): Future[AsyncBucket] =
    observable2FutureWithConvert(cluster.openBucket)

  def openBucket(name: String)(implicit ec: ExecutionContext): Future[AsyncBucket] =
    observable2FutureWithConvert(cluster.openBucket(name))

  def openBucket(name: String, password: String)(implicit ec: ExecutionContext): Future[AsyncBucket] =
    observable2FutureWithConvert(cluster.openBucket(name, password))

  def clusterManager(username: String, password: String)(implicit ec: ExecutionContext): Future[AsyncClusterManager] =
    observable2FutureWithConvert(cluster.clusterManager(username, password))

  def disconnect(implicit ec: ExecutionContext): Future[Boolean] =
    observable2FutureWithConvert(cluster.disconnect)

}
