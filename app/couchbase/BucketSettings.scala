package couchbase

import com.couchbase.client.java.bucket.BucketType
import com.couchbase.client.java.cluster.{ BucketSettings => JavaBucketSettings }

case class BucketSettings(
  name:          String,
  `type`:        BucketType = BucketType.COUCHBASE,
  quota:         Int        = 0,
  port:          Int        = 0,
  password:      String     = "",
  replicas:      Int        = 0,
  indexReplicas: Boolean    = false,
  enableFlush:   Boolean    = false
) extends JavaBucketSettings

object BucketSettings {
  implicit def toScalaBucketSettings(jbs: JavaBucketSettings): BucketSettings =
    BucketSettings(jbs.name, jbs.`type`, jbs.quota, jbs.port, jbs.password, jbs.replicas, jbs.indexReplicas, jbs.enableFlush)
}
