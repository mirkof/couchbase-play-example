package couchbase

import com.couchbase.client.java.document.RawJsonDocument
import com.couchbase.client.java.{AsyncBucket => JavaAsyncBucket}
import couchbase.documents.JsDocument._
import couchbase.documents.{JsDocument, LongDocument}
import couchbase.types._
import couchbase.utils.RxPlayConversions._
import couchbase.view.{AsyncViewResult, ViewQuery}

import scala.concurrent.{ExecutionContext, Future}

class AsyncBucket(bucket: JavaAsyncBucket) {

  def name: String = bucket.name

  def get(id: CouchbaseID)(implicit ec: ExecutionContext): Future[Option[JsDocument]] =
    observable2FutureOptionWithConvert(bucket.get(id.value, classOf[RawJsonDocument]))

  def get(d: JsDocument)(implicit ec: ExecutionContext): Future[Option[JsDocument]] =
    observable2FutureOptionWithConvert(bucket.get[RawJsonDocument](d))

  def getAndLock(id: CouchbaseID, lockTime: Int)(implicit ec: ExecutionContext): Future[Option[JsDocument]] =
    observable2FutureOptionWithConvert(bucket.getAndLock(id.value, lockTime, classOf[RawJsonDocument]))

  def getAndLock(d: JsDocument, lockTime: Int)(implicit ec: ExecutionContext): Future[Option[JsDocument]] =
    observable2FutureOptionWithConvert(bucket.getAndLock[RawJsonDocument](d, lockTime))

  def getAndTouch(id: CouchbaseID, expiry: Expiry)(implicit ec: ExecutionContext): Future[Option[JsDocument]] =
    observable2FutureOptionWithConvert(bucket.getAndTouch(id.value, expiry.value, classOf[RawJsonDocument]))

  def getAndTouch(d: JsDocument)(implicit ec: ExecutionContext): Future[Option[JsDocument]] =
    observable2FutureOptionWithConvert(bucket.getAndTouch[RawJsonDocument](d))

  def insert(d: JsDocument)(implicit ec: ExecutionContext): Future[JsDocument] =
    observable2FutureWithConvert(bucket.insert[RawJsonDocument](d))

  def upsert(d: JsDocument)(implicit ec: ExecutionContext): Future[JsDocument] =
    observable2FutureWithConvert(bucket.upsert[RawJsonDocument](d))

  def replace(d: JsDocument)(implicit ec: ExecutionContext): Future[JsDocument] =
    observable2FutureWithConvert(bucket.replace[RawJsonDocument](d))

  def remove(d: JsDocument)(implicit ec: ExecutionContext): Future[JsDocument] =
    observable2FutureWithConvert(bucket.remove[RawJsonDocument](d))

  def remove(id: CouchbaseID)(implicit ec: ExecutionContext): Future[JsDocument] =
    observable2FutureWithConvert(bucket.remove(id.value))

  def unlock(id: CouchbaseID, cas: CAS)(implicit ec: ExecutionContext): Future[Boolean] =
    observable2FutureWithConvert(bucket.unlock(id.value, cas.value))

  def unlock(d: JsDocument)(implicit ec: ExecutionContext): Future[Boolean] =
    observable2FutureWithConvert(bucket.unlock[RawJsonDocument](d))

  def touch(id: CouchbaseID, expiry: Expiry)(implicit ec: ExecutionContext): Future[Boolean] =
    observable2FutureWithConvert(bucket.touch(id.value, expiry.value))

  def touch(d: JsDocument)(implicit ec: ExecutionContext): Future[Boolean] =
    observable2FutureWithConvert(bucket.touch[RawJsonDocument](d))

  def query(query: ViewQuery)(implicit ec: ExecutionContext): Future[AsyncViewResult] =
    observable2FutureWithConvert(bucket.query(query.jvq))

  def counter(id: CouchbaseID, delta: Long)(implicit ec: ExecutionContext): Future[LongDocument] =
    observable2FutureWithConvert(bucket.counter(id, delta))

  def counter(id: CouchbaseID, delta: Long, initial: Long)(implicit ec: ExecutionContext): Future[LongDocument] =
    observable2FutureWithConvert(bucket.counter(id, delta, initial))

  def counter(id: CouchbaseID, delta: Long, initial: Long, expiry: Expiry)(implicit ec: ExecutionContext): Future[LongDocument] =
    observable2FutureWithConvert(bucket.counter(id, delta, initial, expiry.value))

  def close()(implicit ec: ExecutionContext): Future[Boolean] =
    observable2FutureWithConvert(bucket.close())

  def manager(implicit ec: ExecutionContext): Future[AsyncBucketManager] =
    observable2FutureWithConvert(bucket.bucketManager)

  private[couchbase] def getMultiple(ids: Seq[CouchbaseID])(implicit ec: ExecutionContext): Future[Seq[JsDocument]] = {
    import rx.Observable
    import rx.lang.scala.ImplicitFunctionConversions.scalaFunction1ToRxFunc1

    import scala.collection.JavaConversions._

    val bulkGet = Observable
      .from(ids)
      .flatMap((id: CouchbaseID) => bucket.get(id.value, classOf[RawJsonDocument]))
      .toList
      .single

    observable2Future(bulkGet).map(_.toIndexedSeq.map(rawJsonDocument2JsDocument))
  }

  private[couchbase] def getIterative(ids: Seq[CouchbaseID])(implicit ec: ExecutionContext): Future[Seq[JsDocument]] =
    Future.sequence {
      ids.map { id =>
        observable2Future(bucket.get(id.value, classOf[RawJsonDocument])).map(rawJsonDocument2JsDocument)
      }
    }

}

object AsyncBucket {
  implicit def toScalaAsyncBucket(bucket: JavaAsyncBucket): AsyncBucket = new AsyncBucket(bucket)
}
